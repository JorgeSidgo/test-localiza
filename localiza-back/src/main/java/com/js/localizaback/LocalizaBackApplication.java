package com.js.localizaback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocalizaBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocalizaBackApplication.class, args);
	}

}
