package com.js.localizaback.controller;

import com.js.localizaback.model.dto.CotizacionDTO;
import com.js.localizaback.model.dto.ResponseDTO;
import com.js.localizaback.model.exception.TipoProductoNotFoundException;
import com.js.localizaback.model.service.CotizacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cotizacion")
public class CotizacionController {

    @Autowired
    CotizacionService cotizacionService;


    @GetMapping("/test")
    public ResponseEntity<ResponseDTO> test() {
        ResponseDTO response = new ResponseDTO();
        HttpStatus status;

        try {
            response.setCode(ResponseDTO.COD_OK);
            response.setMessage(ResponseDTO.MSG_OK);
            response.setType(ResponseDTO.RES_TYP_SUCCESS);
            response.setResult("yeahPaps");
            status = HttpStatus.OK;
        } catch (Exception ex) {
            response.setCode(ResponseDTO.COD_ERR);
            response.setMessage(ex.getMessage());
            response.setType(ResponseDTO.RES_TYP_ERROR);
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(response, status);
    }

    @PostMapping
    public ResponseEntity<ResponseDTO> cotizar(
            @RequestBody CotizacionDTO cotizacionDTO
            ) throws TipoProductoNotFoundException {
        ResponseDTO response = new ResponseDTO();
        HttpStatus status;

        try {
            response.setCode(ResponseDTO.COD_OK);
            response.setMessage(ResponseDTO.MSG_OK);
            response.setType(ResponseDTO.RES_TYP_SUCCESS);
            response.setResult(cotizacionService.calcularCotizacion(cotizacionDTO));
            status = HttpStatus.OK;
        } catch (Exception ex) {

            response.setCode(ResponseDTO.COD_ERR);
            response.setMessage(ex.getMessage());
            response.setType(ResponseDTO.RES_TYP_ERROR);
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            throw ex;
        }
        return new ResponseEntity<>(response, status);
    }

    @GetMapping("/list-tipo-producto")
    public ResponseEntity<ResponseDTO> listaTipoProducto() {
        ResponseDTO response = new ResponseDTO();
        HttpStatus status;

        try {
            response.setCode(ResponseDTO.COD_OK);
            response.setMessage(ResponseDTO.MSG_OK);
            response.setType(ResponseDTO.RES_TYP_SUCCESS);
            response.setResult(cotizacionService.listarTipoProducto());
            status = HttpStatus.OK;
        } catch (Exception ex) {
            response.setCode(ResponseDTO.COD_ERR);
            response.setMessage(ex.getMessage());
            response.setType(ResponseDTO.RES_TYP_ERROR);
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(response, status);
    }
}
