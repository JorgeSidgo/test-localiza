package com.js.localizaback.model.repository;

import com.js.localizaback.model.entity.TipoProducto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TipoProductoRepository extends BaseRepository<TipoProducto, Long> {

    @Query("SELECT tp FROM TipoProducto tp where tp.codTipoProducto = :id")
    TipoProducto get(@Param("id") long id);

}
