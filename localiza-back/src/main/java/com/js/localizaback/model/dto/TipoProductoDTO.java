package com.js.localizaback.model.dto;

import com.js.localizaback.model.entity.TipoProducto;

import java.math.BigDecimal;

public class TipoProductoDTO {
    private long codTipoProducto;
    private String nombreTipoProducto;

    public TipoProductoDTO() {
    }

    public TipoProductoDTO(long codTipoProducto, String nombreTipoProducto) {
        this.codTipoProducto = codTipoProducto;
        this.nombreTipoProducto = nombreTipoProducto;
    }

    public static TipoProductoDTO fromEntity(TipoProducto tp) {
        return new TipoProductoDTO(
                tp.getCodTipoProducto(),
                tp.getNombreTipoProducto()
        );
    }

    public long getCodTipoProducto() {
        return codTipoProducto;
    }

    public void setCodTipoProducto(long codTipoProducto) {
        this.codTipoProducto = codTipoProducto;
    }

    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }
}
