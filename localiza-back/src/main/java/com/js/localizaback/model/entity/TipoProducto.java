package com.js.localizaback.model.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "tipo_producto", schema = "localiza")
public class TipoProducto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codTipoProducto;

    @Column(name = "nombre_tipo_producto")
    private String nombreTipoProducto;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "porcentaje_flete")
    private BigDecimal porcentajeFlete;

    @Column(name = "porcentaje_combustible")
    private BigDecimal porcentajeCombustible;

    @Column(name = "porcentaje_seguro")
    private BigDecimal porcentajeSeguro;

    @Column(name = "porcentaje_aduanal")
    private BigDecimal porcentajeAduanal;

    @Column(name = "porcentaje_impuestos")
    private BigDecimal porcentajeImpuestos;

    @Column(name = "cobrar_iva")
    private boolean cobrarIva;

    public TipoProducto() {
    }

    public TipoProducto(long codTipoProducto, String nombreTipoProducto, String descripcion, BigDecimal porcentajeFlete, BigDecimal porcentajeCombustible, BigDecimal porcentajeSeguro, BigDecimal porcentajeAduanal, BigDecimal porcentajeImpuestos, boolean cobrarIva) {
        this.codTipoProducto = codTipoProducto;
        this.nombreTipoProducto = nombreTipoProducto;
        this.descripcion = descripcion;
        this.porcentajeFlete = porcentajeFlete;
        this.porcentajeCombustible = porcentajeCombustible;
        this.porcentajeSeguro = porcentajeSeguro;
        this.porcentajeAduanal = porcentajeAduanal;
        this.porcentajeImpuestos = porcentajeImpuestos;
        this.cobrarIva = cobrarIva;
    }

    public long getCodTipoProducto() {
        return codTipoProducto;
    }

    public void setCodTipoProducto(long codTipoProducto) {
        this.codTipoProducto = codTipoProducto;
    }

    public String getNombreTipoProducto() {
        return nombreTipoProducto;
    }

    public void setNombreTipoProducto(String nombreTipoProducto) {
        this.nombreTipoProducto = nombreTipoProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPorcentajeFlete() {
        return porcentajeFlete;
    }

    public void setPorcentajeFlete(BigDecimal porcentajeFlete) {
        this.porcentajeFlete = porcentajeFlete;
    }

    public BigDecimal getPorcentajeCombustible() {
        return porcentajeCombustible;
    }

    public void setPorcentajeCombustible(BigDecimal porcentajeCombustible) {
        this.porcentajeCombustible = porcentajeCombustible;
    }

    public BigDecimal getPorcentajeSeguro() {
        return porcentajeSeguro;
    }

    public void setPorcentajeSeguro(BigDecimal porcentajeSeguro) {
        this.porcentajeSeguro = porcentajeSeguro;
    }

    public BigDecimal getPorcentajeAduanal() {
        return porcentajeAduanal;
    }

    public void setPorcentajeAduanal(BigDecimal porcentajeAduanal) {
        this.porcentajeAduanal = porcentajeAduanal;
    }

    public BigDecimal getPorcentajeImpuestos() {
        return porcentajeImpuestos;
    }

    public void setPorcentajeImpuestos(BigDecimal porcentajeImpuestos) {
        this.porcentajeImpuestos = porcentajeImpuestos;
    }

    public boolean isCobrarIva() {
        return cobrarIva;
    }

    public void setCobrarIva(boolean cobrarIva) {
        this.cobrarIva = cobrarIva;
    }
}
