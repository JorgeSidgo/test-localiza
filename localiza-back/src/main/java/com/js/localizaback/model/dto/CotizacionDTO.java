package com.js.localizaback.model.dto;

import java.math.BigDecimal;

public class CotizacionDTO {
    private String cliente;
    private String contacto;
    private long codTipoProducto;
    private String tipoProducto;
    private BigDecimal pesoProducto;
    private BigDecimal precioProducto;
    private BigDecimal flete;
    private BigDecimal combustible;
    private BigDecimal seguro;
    private BigDecimal aduanas;
    private BigDecimal impuestos;
    private BigDecimal iva;
    private BigDecimal cargosImportacion;
    private BigDecimal total;

    public CotizacionDTO() {
    }

    public CotizacionDTO(String cliente, String contacto, long codTipoProducto, String tipoProducto, BigDecimal pesoProducto, BigDecimal precioProducto, BigDecimal flete, BigDecimal combustible, BigDecimal seguro, BigDecimal aduanas, BigDecimal impuestos, BigDecimal iva, BigDecimal cargosImportacion, BigDecimal total) {
        this.cliente = cliente;
        this.contacto = contacto;
        this.codTipoProducto = codTipoProducto;
        this.tipoProducto = tipoProducto;
        this.pesoProducto = pesoProducto;
        this.precioProducto = precioProducto;
        this.flete = flete;
        this.combustible = combustible;
        this.seguro = seguro;
        this.aduanas = aduanas;
        this.impuestos = impuestos;
        this.iva = iva;
        this.cargosImportacion = cargosImportacion;
        this.total = total;
    }

    public BigDecimal calcTotalImportacion() {
        return new BigDecimal(
                (
                    this.getFlete().doubleValue() +
                    this.getCombustible().doubleValue() +
                    this.getSeguro().doubleValue() +
                    this.getAduanas().doubleValue() +
                    this.getImpuestos().doubleValue() +
                    this.getIva().doubleValue()
                )
        );
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public long getCodTipoProducto() {
        return codTipoProducto;
    }

    public void setCodTipoProducto(long codTipoProducto) {
        this.codTipoProducto = codTipoProducto;
    }

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public BigDecimal getPesoProducto() {
        return pesoProducto;
    }

    public void setPesoProducto(BigDecimal pesoProducto) {
        this.pesoProducto = pesoProducto;
    }

    public BigDecimal getPrecioProducto() {
        return precioProducto;
    }

    public void setPrecioProducto(BigDecimal precioProducto) {
        this.precioProducto = precioProducto;
    }

    public BigDecimal getFlete() {
        return flete;
    }

    public void setFlete(BigDecimal flete) {
        this.flete = flete;
    }

    public BigDecimal getCombustible() {
        return combustible;
    }

    public void setCombustible(BigDecimal combustible) {
        this.combustible = combustible;
    }

    public BigDecimal getSeguro() {
        return seguro;
    }

    public void setSeguro(BigDecimal seguro) {
        this.seguro = seguro;
    }

    public BigDecimal getAduanas() {
        return aduanas;
    }

    public void setAduanas(BigDecimal aduanas) {
        this.aduanas = aduanas;
    }

    public BigDecimal getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(BigDecimal impuestos) {
        this.impuestos = impuestos;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    public BigDecimal getCargosImportacion() {
        return cargosImportacion;
    }

    public void setCargosImportacion(BigDecimal cargosImportacion) {
        this.cargosImportacion = cargosImportacion;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
