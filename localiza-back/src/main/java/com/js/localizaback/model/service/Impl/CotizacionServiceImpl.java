package com.js.localizaback.model.service.Impl;

import com.js.localizaback.model.dto.CotizacionDTO;
import com.js.localizaback.model.dto.TipoProductoDTO;
import com.js.localizaback.model.entity.TipoProducto;
import com.js.localizaback.model.exception.TipoProductoNotFoundException;
import com.js.localizaback.model.repository.TipoProductoRepository;
import com.js.localizaback.model.service.CotizacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CotizacionServiceImpl implements CotizacionService {

    @Autowired
    TipoProductoRepository tipoProductoRepository;


    @Override
    public CotizacionDTO calcularCotizacion(CotizacionDTO cotizacion) throws TipoProductoNotFoundException {

        try {

            TipoProducto tp = tipoProductoRepository.get(cotizacion.getCodTipoProducto());

            if (tp == null) {
                throw new TipoProductoNotFoundException();
            }

            cotizacion.setTipoProducto(tp.getNombreTipoProducto());

            cotizacion.setFlete(
                    cotizacion
                            .getPrecioProducto()
                            .multiply(tp.getPorcentajeFlete())
                            .setScale(2, RoundingMode.HALF_EVEN)
            );

            cotizacion.setCombustible(
                    cotizacion
                            .getPrecioProducto()
                            .multiply(tp.getPorcentajeCombustible())
                            .setScale(2, RoundingMode.HALF_EVEN)
            );

            cotizacion.setSeguro(
                    cotizacion
                            .getPrecioProducto()
                            .multiply(tp.getPorcentajeSeguro())
                            .setScale(2, RoundingMode.HALF_EVEN)

            );

            cotizacion.setAduanas(
                    cotizacion
                            .getPrecioProducto()
                            .multiply(tp.getPorcentajeAduanal())
                            .setScale(2, RoundingMode.HALF_EVEN)
            );

            cotizacion.setImpuestos(
                    cotizacion
                            .getPrecioProducto()
                            .multiply(tp.getPorcentajeImpuestos())
                            .setScale(2, RoundingMode.HALF_EVEN)
            );

            BigDecimal iva = (tp.isCobrarIva())
                                ? cotizacion.getPrecioProducto().multiply(new BigDecimal(0.13))
                                    .setScale(2, RoundingMode.HALF_EVEN)
                                : new BigDecimal(0.0).setScale(2, RoundingMode.HALF_EVEN);

            cotizacion.setIva(iva);

            cotizacion.setCargosImportacion(
                cotizacion.calcTotalImportacion()
                            .setScale(2, RoundingMode.HALF_EVEN)
            );

            cotizacion.setTotal(
                    cotizacion
                            .getPrecioProducto()
                            .add(cotizacion.getCargosImportacion())
                            .setScale(2, RoundingMode.HALF_EVEN)
            );
        } catch (Exception ex) {
            throw ex;
        }

        return cotizacion;
    }

    @Override
    public List<TipoProductoDTO> listarTipoProducto() {
        return tipoProductoRepository
                .findAll()
                .stream()
                .map(TipoProductoDTO::fromEntity)
                .collect(Collectors.toList());
    }
}
