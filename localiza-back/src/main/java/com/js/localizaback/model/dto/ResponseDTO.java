package com.js.localizaback.model.dto;

public class ResponseDTO {
    // RESPONSE MESSAGE & CODE
    public static String COD_OK = "00";
    public static String MSG_OK = "Procesamiento exitoso";

    public static String COD_ERR = "99";
    public static String MSG_ERR = "Error de procesamiento";

    public static String COD_AUTH_OK = "01";
    public static String MSG_AUTH_OK = "Autorizado";

    public static String COD_AUTH_DND = "02";
    public static String MSG_AUTH_DND = "Datos Incorrectos";

    // RESPONSE TYPE

    public static String RES_TYP_SUCCESS = "success";
    public static String RES_TYP_ERROR = "error";
    public static String RES_TYP_INFO = "info";
    public static String RES_TYP_WARNING = "warning";

    private String code;
    private String type;
    private int status;
    private String message;
    private Object errors;
    private Object result;

    public ResponseDTO() {
    }

    public ResponseDTO(String code, String type, int status, String message, Object errors, Object result) {
        this.code = code;
        this.type = type;
        this.status = status;
        this.message = message;
        this.errors = errors;
        this.result = result;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getErrors() {
        return errors;
    }

    public void setErrors(Object errors) {
        this.errors = errors;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
