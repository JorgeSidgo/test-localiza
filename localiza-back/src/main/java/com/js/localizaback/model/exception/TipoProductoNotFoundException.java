package com.js.localizaback.model.exception;

public class TipoProductoNotFoundException extends Exception{

    public TipoProductoNotFoundException() {
        super("Tipo de Producto no Encontrado");
    }

}
