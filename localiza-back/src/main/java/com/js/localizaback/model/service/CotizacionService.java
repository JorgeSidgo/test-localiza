package com.js.localizaback.model.service;

import com.js.localizaback.model.dto.CotizacionDTO;
import com.js.localizaback.model.dto.TipoProductoDTO;
import com.js.localizaback.model.exception.TipoProductoNotFoundException;

import java.util.List;

public interface CotizacionService {
    CotizacionDTO calcularCotizacion(CotizacionDTO cotizacion) throws TipoProductoNotFoundException;
    List<TipoProductoDTO> listarTipoProducto();
}
