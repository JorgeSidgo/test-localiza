export class Cotizacion {
    cliente: string;
    contacto: string;
    codTipoProducto: number;
    tipoProducto: string;
    pesoProducto: number;
    precioProducto: number;
    flete: number;
    combustible: number;
    seguro: number;
    aduanas: number;
    impuestos: number;
    iva: number;
    cargosImportacion: number;
    total: number;
}