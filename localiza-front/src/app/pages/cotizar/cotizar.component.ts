import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd';
import { CotizarService } from 'src/app/services/cotizar.service';
import { Cotizacion } from 'src/app/types/cotizacion';

@Component({
  selector: 'app-cotizar',
  templateUrl: './cotizar.component.html',
  styleUrls: ['./cotizar.component.css']
})
export class CotizarComponent implements OnInit {

  isLoading = false;
  cotizarForm: FormGroup;
  listTipoProducto = new Array();
  resultado = false;
  cotizacion: Cotizacion = new Cotizacion();

  constructor(
    private message: NzMessageService,
    private cotizarService: CotizarService,
    private fb: FormBuilder 
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.listaTipoProducto();
  }

  initForm(): void {
    this.cotizarForm = this.fb.group({
      cliente: [null, [Validators.required]],
      contacto: [null, [Validators.required, Validators.email]],
      codTipoProducto: [null, [Validators.required]],
      pesoProducto: [null, [Validators.required]],
      precioProducto: [null, [Validators.required]]
    });
  }

  listaTipoProducto(): void {
    this.cotizarService.listadoTipoProducto().subscribe(data => {
      this.listTipoProducto = data.result;
    })
  }

  handleOk(): void {
    this.isLoading = true;

    // tslint:disable-next-line: forin
    for (const i in this.cotizarForm.controls) {
      this.cotizarForm.controls[i].markAsDirty();
      this.cotizarForm.controls[i].updateValueAndValidity();
    }

    if (this.cotizarForm.dirty && this.cotizarForm.valid) {
      this.cotizarService.cotizar(this.cotizarForm.value).subscribe((data) => {
        this.isLoading = false;
        if (data.code) {
          this.message.success('Cotizacion exitosa');
          this.cotizacion = data.result;
          this.resultado = true;

        } else {
          this.isLoading = false;
          this.message.error(data.message);
        }
      }, (error) => {

        if (error.status === 400) {
          this.message.error(`${error}`);
        } else {
          this.message.error('Error en la petición');
          console.log(error);
        }

        this.isLoading = false;
      });
    } else {
      this.message.warning('Complete el formulario');
      this.isLoading = false;
    }



  }

}
