import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CotizarComponent } from './cotizar.component';

const routes: Routes = [
  { path: '', component: CotizarComponent },
  { path: '**', component: CotizarComponent },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CotizarRoutingModule { }
