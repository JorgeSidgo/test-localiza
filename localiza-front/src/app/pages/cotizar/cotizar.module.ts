import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CotizarRoutingModule } from './cotizar-routing.module';
import { CotizarComponent } from './cotizar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { es_ES, NgZorroAntdModule, NZ_I18N } from 'ng-zorro-antd';


@NgModule({
  declarations: [
    CotizarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    CotizarRoutingModule
  ],
  exports: [
    CotizarComponent,
    NgZorroAntdModule,
    ReactiveFormsModule,
  ],
  
  providers: [{ provide: NZ_I18N, useValue: es_ES }],
})
export class CotizarModule { }
