import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CotizarService {

  baseUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.baseUrl = `${environment.baseUrl}/cotizacion`;
  }

  listadoTipoProducto(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/list-tipo-producto`).pipe(
      map((response: any) => response)
    );
  }

  cotizar(cotizacion: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}`, cotizacion).pipe(
      map((response: any) => response)
    );
  }
}
