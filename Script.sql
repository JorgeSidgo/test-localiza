create database if not exists localiza;
use localiza;

create user 'localiza'@'localhost' identified by 'Overflow7020';
grant all privileges on localiza.* to 'localiza'@'localhost';

create table tipo_producto (
	cod_tipo_producto bigint auto_increment not null,
	nombre_tipo_producto varchar(100),
	descripcion varchar(500),
	porcentaje_flete decimal(16, 4),
	porcentaje_combustible decimal(16, 4),
	porcentaje_seguro decimal(16, 4),
	porcentaje_aduanal decimal(16, 4),
	porcentaje_impuestos decimal(16, 4),
	cobrar_iva tinyint,
	
	primary key(cod_tipo_producto)
);

insert into tipo_producto
	(
		nombre_tipo_producto, 
		descripcion,
		porcentaje_flete, 
		porcentaje_combustible, 
		porcentaje_seguro, 
		porcentaje_aduanal, 
		porcentaje_impuestos,
		cobrar_iva
	)
values 
	(
		'Celular',
		'Productos telef�nicos y relacionados',
		0.07,
		0.02,
		0.05,
		0.04,
		0.15,
		1
	);
	
insert into tipo_producto
	(
		nombre_tipo_producto, 
		descripcion,
		porcentaje_flete, 
		porcentaje_combustible, 
		porcentaje_seguro, 
		porcentaje_aduanal, 
		porcentaje_impuestos,
		cobrar_iva
	)
values 
	(
		'Ropa',
		'Ropa y art�culos de vestimenta',
		0.02,
		0.01,
		0.03,
		0.05,
		0.12,
		0
	);
	
insert into tipo_producto
	(
		nombre_tipo_producto, 
		descripcion,
		porcentaje_flete, 
		porcentaje_combustible, 
		porcentaje_seguro, 
		porcentaje_aduanal, 
		porcentaje_impuestos,
		cobrar_iva
	)
values 
	(
		'Repuestos de Veh�culo',
		'Repuestos y partes automotrices',
		0.10,
		0.03,
		0.05,
		0.06,
		0.12,
		1
	);
	
select * from localiza.tipo_producto tp 